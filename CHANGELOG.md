# gitlab-pgsql-ha CHANGELOG

This file is used to list changes made in each version of the gitlab-pgsql-ha cookbook.

## 0.1.2
Drop chef search and use a nodelist hash

## 0.1.1
Use gitlab-vault and updated license

## 0.1.0
Initial release of gitlab-pgsql-ha
