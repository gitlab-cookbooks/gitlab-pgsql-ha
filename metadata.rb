name             'gitlab-pgsql-ha'
maintainer       'GitLab Inc.'
maintainer_email 'jeroen@gitlab.com'
license          'MIT'
description      'Installs/Configures gitlab-pgsql-ha'
long_description IO.read(File.join(File.dirname(__FILE__), 'README.md'))
version          '0.1.2'

depends 'gitlab-vault'
