default['gitlab-pgsql-ha']['port'] = '5432'
default['gitlab-pgsql-ha']['monitor_port'] = '5433'
default['gitlab-pgsql-ha']['user'] = 'gitlab_replicator'
default['gitlab-pgsql-ha']['ringnumber'] = '0'
default['gitlab-pgsql-ha']['authkey'] = 'override_attribute in vault!'
default['gitlab-pgsql-ha']['nodelist'] = []
# default['gitlab-pgsql-ha']['nodelist'] = ['127.0.0.1','127.0.0.2']
